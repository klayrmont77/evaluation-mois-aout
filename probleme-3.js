function pairProgramming(experiences, isMostExperienced) {
  let firstExperiencedIndex, secondExperiencedIndex;
  if (isMostExperienced) {
    firstExperiencedIndex = experiences.indexOf(Math.max(...experiences));
    experiences.splice(firstExperiencedIndex, 1);
    secondExperiencedIndex = experiences.indexOf(Math.max(...experiences));
  } else {
    firstExperiencedIndex = experiences.indexOf(Math.min(...experiences));
    experiences.splice(firstExperiencedIndex, 1);
    secondExperiencedIndex = experiences.indexOf(Math.min(...experiences));
  }
  if (firstExperiencedIndex < secondExperiencedIndex)
    secondExperiencedIndex += 1;
  return [
    Math.min(firstExperiencedIndex, secondExperiencedIndex),
    Math.max(firstExperiencedIndex, secondExperiencedIndex),
  ];
}

console.log(pairProgramming([1, 4, 3, 2, 8, 3], false));
